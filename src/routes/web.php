<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 07.11.17
 * Time: 18:55
 */

Route::group(['prefix' => 'oauth', 'middleware' => 'web'], function () {
    Route::get('happeak', [
        'as'   => 'oauth',
        'uses' => 'Happeak\Oauth\Controllers\OauthController@redirectToProvider',
    ]);
    Route::get('happeak/callback', [
        'as'   => 'oauth_callback',
        'uses' => 'Happeak\Oauth\Controllers\OauthController@handleProviderCallback',
    ]);
});