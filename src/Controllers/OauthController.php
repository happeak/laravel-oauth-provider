<?php

namespace Happeak\Oauth\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class OauthController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @param $provider
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @param $provider
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        switch ($provider) {
            case 'happeak':

                $user = User::updateOrCreate(
                    [
                        'email' => $user->email,
                    ],
                    [
                        'name'     => $user->name,
                        'password' => Hash::make(uniqid(mt_rand(), true)),
                        'bonus'    => $user->bonus,
                    ]
                );

                Auth::login($user, true);

                return redirect()->intended('/');
                break;
            default:
                abort(404, 'Selected driver is not supported.');
                break;
        }
    }
}