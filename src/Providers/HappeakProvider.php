<?php

namespace Happeak\Oauth\Providers;

use Illuminate\Http\Request;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class HappeakProvider extends AbstractProvider implements ProviderInterface
{

    /**
     * OAuth domain. Needed sometimes. For example for testing on local domains
     *
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $domain;

    /**
     * @var [temporary] нужно для смены url до профиля пользователя до тех пор, пока не будет работать ecommerce
     */
    protected $profileUrl;

    public function __construct(Request $request, $clientId, $clientSecret, $redirectUrl, array $guzzle = [])
    {
        parent::__construct($request, $clientId, $clientSecret, $redirectUrl, $guzzle);

        $this->domain = config('services.happeak.domain', 'https://www.happeak.ru');
        $this->profileUrl = config('services.happeak.profile_url', $this->domain . '/api/profile');
    }

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase("{$this->domain}/oauth/authorize", $state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return "{$this->domain}/oauth/token";
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return array_add(
            parent::getTokenFields($code), 'grant_type', 'authorization_code'
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get($this->profileUrl, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'         => $user['id'],
            'name'       => $user['first_name'],
            'email'      => $user['email'],
            'bonus'      => $user['bonus'],
            'created_at' => $user['created_at'],
        ]);
    }
}
