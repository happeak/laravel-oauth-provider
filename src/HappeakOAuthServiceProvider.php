<?php

namespace Happeak\Oauth;

use Happeak\Oauth\Providers\HappeakProvider;
use Illuminate\Support\ServiceProvider;

class HappeakOAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        $this->bootHappeakOauthProvider();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Расширяем Socialite, добавляя провайдер для OAuth авторизации в Happeak
     */
    public function bootHappeakOauthProvider()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'happeak',
            function ($app) use ($socialite) {
                $config = $app['config']['services.happeak'];

                return $socialite->buildProvider(HappeakProvider::class, $config);
            }
        );
    }
}
