# Happeak Laravel Oauth Provider

Это простой пакет для быстрой интеграции новых проектов на laravel с Happeak OAuth. 

## Установка
```
composer require happeak/laravel-oauth-provider
```

Далее в файле config/services.php добавить: 

```php
'happeak' => [
        'client_id'     => env('HAPPEAK_CLIENT_ID'),
        'client_secret' => env('HAPPEAK_CLIENT_SECRET'),
        'redirect'      => env('HAPPEAK_OAUTH_REDIRECT'),
        'domain'        => env('HAPPEAK_DOMAIN'),
    ],
```

После чего добавить недостающие ключи в файл .env, лежащий в корне проекта.